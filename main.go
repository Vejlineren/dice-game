package main

import (
	"flag"
	"fmt"
	"math/rand"
	"time"
)

// Class for each die
type die struct {
	max int
	min int

	faceValue int
}

// Class for the diceGame
type diceGame struct {
	die1 die
	die2 die

	totalFaceValue int

	wins   int
	losses int
}

// Function for rolling ONE die
func (d *die) roll() {
	// The "+1" is because rand.Intn(maxValue) => random number < 6 and not =< 6. So it's off by one.
	// The last "+ d.min" is of course to add the minimum value of the dice.
	d.faceValue = rand.Intn(d.max-d.min+1) + d.min
}

// Constructor for die
func (d *die) constructdie(dieSize int) {
	d.max = dieSize
	d.min = 1
}

// Function for returning the total face value
func (game diceGame) getFaceValue() int {
	return game.die1.faceValue + game.die2.faceValue
}

// Definition for game progress
func (game *diceGame) play(d int) int {
	// Construct dice
	game.die1.constructdie(d)
	game.die2.constructdie(d)

	//2. Roll both dice
	game.die1.roll()
	game.die2.roll()

	//3. Get total face value
	faceValue := game.getFaceValue()

	return faceValue
}

func (game *diceGame) gameRoutine(d int, r int) {
	// Print game settings
	fmt.Println("**Game settings**")
	fmt.Printf("Sides of die: %d \n", d)
	fmt.Printf("Number of throws: %d \n \n", r)

	// Run game r number of times and register wins/losses
	for i := 0; i < r; i++ {
		value := game.play(d)

		if value == 7 {
			game.wins++
		} else {
			game.losses++
		}
	}
}

func (game diceGame) score(r int) {
	// Print title
	fmt.Println("**Game results**")

	// Print wins/losses
	fmt.Printf("Number of wins: %d \n", game.wins)
	fmt.Printf("Number of losses: %d \n", game.losses)

	// Calculate percentage
	winsPercent := (float64(game.wins) / float64(r) * 100)
	lossPercent := (float64(game.losses) / float64(r) * 100)

	// Print percentage wins/losses
	fmt.Printf("%% of wins: %f \n", winsPercent)
	fmt.Printf("%% of losses: %f \n", lossPercent)
}

func main() {
	// Set flags
	dieFlag := flag.Int("die", 6, "Adjust number of sides on die")
	throwsFlag := flag.Int("throws", 1000, "Adjust number of throws")
	flag.Parse()

	// Generate random seed to avoid same sequence of pseudo random numbers
	rand.Seed(time.Now().UnixNano())

	// Create instance of diceGame
	game := diceGame{}

	// Run the game 1000 times and keep track of wins/losses
	game.gameRoutine(*dieFlag, *throwsFlag)

	// Show the scores
	game.score(*throwsFlag)

}
