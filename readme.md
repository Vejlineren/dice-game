# Dice-Game
This repository contains a simple Go programme simulating a game of rolling dice.
The rules are:
- A player throws two dice of X sides Y number of times
- If the total face value is == 7, then the player wins 
- If the total face value is != 7, then the player losses

## Running the programme
1. cd path/to/directory
2. go build
3. ./directory -h
4. ./directory -die X -throws Y
